#include "TSS_List.h"

//从指定地址偏移一定的长度，进行数据读写
#define MEM_MOVE_8(p, size) *((volatile uint8_t *)p + (size))
#define MEM_MOVE_16(p, size) *((volatile uint16_t *)p + (size) / 2)
#define MEM_MOVE_32(p, size) *((volatile uint32_t *)p + (size) / 4)
//偏移的重要地址
#define MEM_SIZE() MEM_MOVE_16(list, 0)                      //内存大小
#define MEM_NUM() MEM_MOVE_16(list, 2)                       //数据个数
#define MEM_LEN(n) MEM_MOVE_16(list, MEM_SIZE() - 2 - 2 * n) //第n个数据的宽度

//计算两个数据之间的偏移量
static uint16_t MEM_OFFSET(void *list, uint16_t n1, uint16_t n2)
{
    uint16_t offset = 0;
    for (uint16_t i = n1; i < n2; i++)
    {
        offset += MEM_LEN(i);
    }
    return offset;
}
//查找第n个数据的地址
static void *MEM_DATA(void *list, uint16_t n)
{
    if (n > MEM_NUM())
        return NULL;
    uint16_t offset = MEM_OFFSET(list, 0, n);
    return (void *)&MEM_MOVE_8(list, 4 + offset);
}
//返回列表剩余空间
static int16_t MEM_SPACE(void *list)
{
    uint16_t offset = MEM_OFFSET(list, 0, MEM_NUM());
    return MEM_SIZE() - 4 - MEM_NUM() * 2 - offset;
}

/**
  * @brief  TSS列表初始化
  * @param  list   管理内存的首地址
  * @param  size   内存的大小,最大支持到65535
  * @retval 返回成功或者失败
  * @note   需提供给列表一块全局内存空间
  *         本模块会在给定的内存空间内操作数据
  */
int8_t TSS_ListInit(void *list, uint16_t size)
{
    if (size < 8)
        return TSS_MEMSIZE;
    memset(list, 0, size);
    MEM_SIZE() = size;
    return TSS_OK;
}

/**
  * @brief  TSS列表插入
  * @param  list   管理内存的首地址
  * @param  data   要插入数据的首地址
  * @param  len    要插入数据的内存大小
  * @param  num    要把数据插入列表中的第几个
  * @retval 返回成功或者失败
  * @note   无
  */
int8_t TSS_ListInsert(void *list, void *data, uint16_t len, uint16_t num)
{
    if (MEM_SPACE(list) < len + 2)
        return TSS_MEMFULL;
    if (num > MEM_NUM())
        return TSS_NODATA;
    memmove((void *)((uint8_t *)MEM_DATA(list, num) + len), MEM_DATA(list, num), MEM_OFFSET(list, num, MEM_NUM()));
    memcpy(MEM_DATA(list, num), data, len);
    memmove((void *)(&MEM_MOVE_8(list, MEM_SIZE() - (MEM_NUM() + 1) * 2)),
            (void *)(&MEM_MOVE_8(list, MEM_SIZE() - MEM_NUM() * 2)),
            (MEM_NUM() - num) * 2);
    MEM_NUM() += 1;
    MEM_LEN(num) = len;
    return TSS_OK;
}

/**
  * @brief  TSS列表擦除
  * @param  list   管理内存的首地址
  * @param  num    要擦除列表中的第几个
  * @retval 返回成功或者失败
  * @note   无
  */
int8_t TSS_ListErase(void *list, uint16_t num)
{
    if (MEM_NUM() == 0)
        return TSS_NODATA;
    if (num > MEM_NUM())
        return TSS_NODATA;
    memmove(MEM_DATA(list, num), MEM_DATA(list, num + 1), MEM_OFFSET(list, num + 1, MEM_NUM()));
    memmove((void *)(&MEM_MOVE_8(list, MEM_SIZE() - (MEM_NUM() - 1) * 2)),
            (void *)(&MEM_MOVE_8(list, MEM_SIZE() - MEM_NUM() * 2)),
            (MEM_NUM() - num - 1) * 2);
    MEM_NUM() -= 1;
    return TSS_OK;
}

/**
  * @brief  访问第n个数据
  * @param  list   管理内存的首地址
  * @param  data   要访问数据的首地址
  * @param  len    要访问数据的内存大小
  * @param  num    要访问的数据序号
  * @retval 返回成功或者失败
  * @note   并不会擦除数据
  */
int8_t TSS_ListAccess(void *list, void *data, uint16_t *len, uint16_t num)
{
    if (num > MEM_NUM())
        return TSS_NODATA;
    memcpy(data, MEM_DATA(list, num), MEM_LEN(num));
    *len = MEM_LEN(num);
    return TSS_OK;
}

/**
  * @brief  返回数据个数
  * @param  list  管理内存的首地址
  * @retval 返回数据个数
  * @note   无
  */
uint16_t TSS_ListSize(void *list)
{
    return MEM_NUM();
}

/**
  * @brief  判断是否列表为空
  * @param  list  管理内存的首地址
  * @retval 返回是或否
  * @note   无
  */
int8_t TSS_ListEmpty(void *list)
{
    return !MEM_NUM();
}

/**
  * @brief  返回列表剩余字节数
  * @param  list  管理内存的首地址
  * @retval 返回队列剩余字节数
  * @note   无
  */
int32_t TSS_ListSpace(void *list)
{
    return MEM_SPACE(list);
}
