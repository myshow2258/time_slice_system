#include "Linux_Config.h"
#include "TSS.h"

//?命令的例子
static uint8_t At_Command_Help(TSS_CommandParameterDef *parameter)
{
    Log_printf("Help", "At_Command_Help\n");
    return 0;
}
//=命令的例子
//可向命令传递参数
static uint8_t At_Command_Example(TSS_CommandParameterDef *parameter)
{
    Log_printf("Exam", "At_Command_Example(%d)%u %u %u %u %u %u %u %u (%s)\r\n", parameter->parameter32count,
               parameter->parameter32[0], parameter->parameter32[1], parameter->parameter32[2], parameter->parameter32[3],
               parameter->parameter32[4], parameter->parameter32[5], parameter->parameter32[6], parameter->parameter32[7],
               parameter->parameterStr);
    return 0;
}

TSS_CommandTableDef At_Command_Tab[] = {
    {"help?", At_Command_Help},
    {"example=", At_Command_Example},
};
const uint8_t At_Command_TabLen = sizeof(At_Command_Tab) / sizeof(TSS_CommandTableDef);

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Usart_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    TSS_CommandInit(At_Command_Tab, At_Command_TabLen);
    /* Add your application code here */

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
