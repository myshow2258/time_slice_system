#include "Linux_Config.h"
#include "TSS.h"

//全局内存
uint8_t Queue[128];

//任务示例
uint8_t EXAMPLE1_TASK_ID = 0;
void Example1_Task(TSS_TaskMessageDef *p)
{
    uint8_t data[128] = {0};
    sprintf(data, "Hello world %d!", TSS_GetRunTime());
    if (TSS_QueuePush(Queue, data, strlen(data) + 1) != TSS_OK)
    {
        Log_printf("Push", "Queue full\n");
    }
    else
    {
        Log_printf("Push", "space = %d, size = %d\n", TSS_QueueSpace(Queue), TSS_QueueSize(Queue));
    }
}
uint8_t EXAMPLE2_TASK_ID = 0;
void Example2_Task(TSS_TaskMessageDef *p)
{
    uint8_t data[128] = {0};
    uint16_t len = 0;
    if (!TSS_QueueEmpty(Queue))
    {
        TSS_QueueFront(Queue, data, &len);
        TSS_QueuePop(Queue);
        Log_printf("Pop1", "data = %s, len = %d\n", data, len);
    }
    else
    {
        Log_printf("Pop1", "Queue empty\n");
    }
}
uint8_t EXAMPLE3_TASK_ID = 0;
void Example3_Task(TSS_TaskMessageDef *p)
{
    uint8_t data[128] = {0};
    uint16_t len = 0;
    if (!TSS_QueueEmpty(Queue))
    {
        TSS_QueueFront(Queue, data, &len);
        TSS_QueuePop(Queue);
        Log_printf("Pop2", "data = %s, len = %d\n", data, len);
    }
    else
    {
        Log_printf("Pop2", "Queue empty\n");
    }
}

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    /* 创建队列 */
    if (TSS_QueueInit(Queue, sizeof(Queue)) != TSS_OK)
    {
        Log_printf("ERROR", "TSS_QueueInit failed!\n");
    }

    /* Add your application code here */
    EXAMPLE1_TASK_ID = TSS_ThreadCreate(Example1_Task, 2, TASK_HZ, 500);
    EXAMPLE2_TASK_ID = TSS_ThreadCreate(Example2_Task, 3, TASK_HZ, 1000);
    EXAMPLE3_TASK_ID = TSS_ThreadCreate(Example3_Task, 1, TASK_HZ, 1400);

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
