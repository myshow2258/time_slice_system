#include "Linux_Config.h"
#include "TSS.h"

//动作播放器结构体
TSS_ActionPlayerDef Action1 = {0};
TSS_ActionPlayerDef Action2 = {0};

//动作列表示例，任意编写动作列表
static int8_t Action_ExcmpleStart(void *parameter);
static int8_t Action_ExcmpleRun(void *parameter);
static int8_t Action_ExcmpleCondition(void *parameter);
static int8_t Action_ExcmpleEnd(void *parameter);
//动作组, 每一行代表一组动作, 可分别填充不同的函数, 以适应不同的情况
TSS_ActionTableDef ActionExcmple1[] = {
    {Action_ExcmpleStart, Action_ExcmpleRun, 50, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action1"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 100, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action1"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 150, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action1"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 200, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action1"},
};
uint16_t ActionExcmpleLen1 = sizeof(ActionExcmple1) / sizeof(TSS_ActionTableDef);
TSS_ActionTableDef ActionExcmple2[] = {
    {Action_ExcmpleStart, Action_ExcmpleRun, 50, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action2"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 100, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action2"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 150, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action2"},
    {Action_ExcmpleStart, Action_ExcmpleRun, 200, Action_ExcmpleCondition, Action_ExcmpleEnd, (void *)"Action2"},
};
uint16_t ActionExcmpleLen2 = sizeof(ActionExcmple2) / sizeof(TSS_ActionTableDef);

//动作示例, 本组函数可定义在任意的地方
//本示例只展示了一组函数
//实际动作编写时可为每一组动作编写不同的函数
static uint32_t Action_ExcmpleParameter = 0;
static int8_t Action_ExcmpleStart(void *parameter)
{
    char* Str = (char *)parameter; //对传递进来的参数进行类型转换
    Action_ExcmpleParameter = 0;
    Log_printf("Start", "(%s)Start %d!\r\n", Str, Action_ExcmpleParameter);
    Action_ExcmpleParameter++;
    return TSS_ACTIONFUN_OK;
}
static int8_t Action_ExcmpleRun(void *parameter)
{
    char* Str = (char *)parameter; //对传递进来的参数进行类型转换
    Log_printf("Run", "(%s)Run %d!\r\n", Str, Action_ExcmpleParameter);
    Action_ExcmpleParameter++;
    return TSS_ACTIONFUN_OK;
}
static int8_t Action_ExcmpleCondition(void *parameter)
{
    char* Str = (char *)parameter; //对传递进来的参数进行类型转换
    if (Action_ExcmpleParameter > 10)       //设定结束条件
    {
        Log_printf("Condi", "(%s)Condition %d!\r\n", Str, Action_ExcmpleParameter);
        return TSS_ACTIONFUN_END;
    }
    else
        return TSS_ACTIONFUN_OK;
}
static int8_t Action_ExcmpleEnd(void *parameter)
{
    char* Str = (char *)parameter; //对传递进来的参数进行类型转换
    Log_printf("End", "(%s)End %d!\r\n", Str, Action_ExcmpleParameter);
    Action_ExcmpleParameter = 0;
    return TSS_ACTIONFUN_OK;
}

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    TSS_ActionCreate(&Action1, 1);
    TSS_ActionCreate(&Action2, 2);
    /* Add your application code here */
    if (TSS_ActionStart(&Action1, ActionExcmple1, ActionExcmpleLen1, 5) != TSS_OK)
    {
        Log_printf("ERROR", "TSS_ActionStart failed!\r\n");
    }
    if (TSS_ActionStart(&Action2, ActionExcmple2, ActionExcmpleLen2, 5) != TSS_OK)
    {
        Log_printf("ERROR", "TSS_ActionStart failed!\r\n");
    }

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
