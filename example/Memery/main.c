#include "Linux_Config.h"
#include "TSS.h"

//全局内存
uint8_t Mem[128];

//任务示例
uint8_t EXAMPLE1_TASK_ID = 0;
void Example1_Task(TSS_TaskMessageDef *p)
{
    char *Str = TSS_MemMalloc(Mem, 20);
    if (Str != NULL)
    {
        sprintf(Str, "Hello world %d!", TSS_GetRunTime());
        Log_printf("malloc", "%s\n", Str);
    }
    else
    {
        Log_printf("malloc", "malloc failed!\n");
    }
    Log_printf("malloc", "space = %d, num = %d\n", TSS_MemSpace(Mem), TSS_MemSize(Mem));

    TSS_MemFree(Mem, Str);
    if (Str != TSS_OK)
    {
        Log_printf("Free", "free success!\n");
    }
    else
    {
        Log_printf("Free", "free failed!\n");
    }
}

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    /* 创建队列 */
    if (TSS_MemInit(Mem, sizeof(Mem)) != TSS_OK)
    {
        Log_printf("ERROR", "TSS_MemInit failed!\n");
    }

    /* Add your application code here */
    EXAMPLE1_TASK_ID = TSS_ThreadCreate(Example1_Task, 1, TASK_HZ, 1000);

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
