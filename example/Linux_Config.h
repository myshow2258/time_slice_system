#ifndef __LINUX_CONFIG_H
#define __LINUX_CONFIG_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

    void Systick_Init(void);
    void Usart_Init(void);

    uint32_t getmstime(void);
    void Log_printf(char *head, const char *format, ...);
    void Hex2Ascii(uint8_t *Data, uint16_t Len, uint8_t *Str);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
