#include "Linux_Config.h"
#include "TSS.h"

//全局内存
uint8_t List[128];

//任务示例
uint8_t EXAMPLE1_TASK_ID = 0;
void Example1_Task(TSS_TaskMessageDef *p)
{
    uint8_t data[128] = {0};
    sprintf(data, "Hello world %d!", TSS_GetRunTime());
    if (TSS_ListInsert(List, data, strlen(data) + 1, TSS_ListSize(List)) != TSS_OK)
    {
        Log_printf("Insert", "List full\n");
    }
    else
    {
        Log_printf("Insert", "space = %d, size = %d\n", TSS_ListSpace(List), TSS_ListSize(List));
    }
}
uint8_t EXAMPLE2_TASK_ID = 0;
void Example2_Task(TSS_TaskMessageDef *p)
{
    if (!TSS_ListEmpty(List))
    {
        TSS_ListErase(List, 0);
        Log_printf("erase", "erase 0\n");
    }
    else
    {
        Log_printf("erase", "List empty\n");
    }
}
uint8_t EXAMPLE3_TASK_ID = 0;
void Example3_Task(TSS_TaskMessageDef *p)
{
    uint8_t data[128] = {0};
    uint16_t len = 0;
    if (!TSS_ListEmpty(List))
    {
        for (uint16_t i = 0; i < TSS_ListSize(List); i++)
        {
            TSS_ListAccess(List, data, &len, i);
            Log_printf("Access", "%d : data = %s, len = %d\n", i, data, len);
        }
    }
    else
    {
        Log_printf("Access", "List empty\n");
    }
}

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    /* 创建队列 */
    if (TSS_ListInit(List, sizeof(List)) != TSS_OK)
    {
        Log_printf("ERROR", "TSS_ListInit failed!\n");
    }

    /* Add your application code here */
    EXAMPLE1_TASK_ID = TSS_ThreadCreate(Example1_Task, 2, TASK_HZ, 250);
    EXAMPLE2_TASK_ID = TSS_ThreadCreate(Example2_Task, 3, TASK_HZ, 500);
    EXAMPLE3_TASK_ID = TSS_ThreadCreate(Example3_Task, 1, TASK_HZ, 1000);

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
