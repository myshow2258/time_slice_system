#include "Linux_Config.h"
#include "TSS.h"

//任务示例
uint8_t EXAMPLE1_TASK_ID = 0;
void Example1_Task(TSS_TaskMessageDef *p)
{
    Log_printf("INFO", "(%d)Example1_Task\n", p->past);
}

/**
  * @brief  任务操作的api示例
  * @retval 无
  * @note   无
  */
int main()
{
    TSS_Init();
    Systick_Init();
    Log_printf("INFO", "Systerm Init ok!\n");

    /* Add your application code here */
    EXAMPLE1_TASK_ID = TSS_ThreadCreate(Example1_Task, 1, TASK_HZ, 500);

    /* Start systerm here */
    TSS_Start();
    /* Infinite loop */
    while (1)
    {
    }

    return 0;
}
