## 例子:
* 本例子使用linux的系统定时器产生类似systick的间隔中断
* 在linux下使用命令:
  * apt install scons
  * apt install gcc
* 在各个例子目录下使用命令 scons 即可编译工程
* 再使用 ./main 查看运行结果
