# Time_slice_system
## 一款时间片轮训的操作系统:
* 1.使用时间片轮训来模拟多任务(所有任务非阻塞).
* 2.免去rtos内核的内存管理.
* 3.一套模仿rtos的api.
* 4.依赖于systick中断.

## 使用方法:
* 本库完全开放源码,直接将源文件添加进编译即可.
* 包含#include "TSS.h" 即包含了本库的所有api.

## 例子的使用方法:
* 例子使用scons + linux进行api的调用测试.
* 存放于example文件夹中

### Have fun!
