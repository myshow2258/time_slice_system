#include "TSS_Command.h"

static TSS_CommandDef TSS_Command = {0};

#define TSS_COMMAND_HEAD "AT+"
#define TSS_COMMAND_HLEN 3

#define TSS_COMMAND_SET "="
#define TSS_COMMAND_SLEN 1

#define TSS_COMMAND_CAT ","
#define TSS_COMMAND_CLEN 1

#define TSS_COMMAND_END "?"
#define TSS_COMMAND_ELEN 1

/**
  * @brief  命令初始化
  * @param  table       需要解析的命令列表
  * @param  num         命令列表中的命令个数
  * @note   请自行创建命令函数与命令结构体列表
  *         使用本函数初始化命令即可得到执行
  *         命令以=结尾，代表需要提供参数，每个参数以,分割
  *         命令以?结尾，代表无需提供参数，直接执行命令
  */
void TSS_CommandInit(TSS_CommandTableDef *table, uint16_t num)
{
    TSS_Command.num = num;
    TSS_Command.table = table;
}

//寻找命令中的参数
static void TSS_CommandParameter(uint8_t *command, uint16_t len, TSS_CommandParameterDef *parameter)
{
    if (strstr((char *)command, TSS_COMMAND_END) != NULL) //如果发现?
    {
        parameter->parameter32count = 0;
    }
    else if (strstr((char *)command, TSS_COMMAND_SET) != NULL) //如果发现=
    {
        char *cat = strstr((char *)command, TSS_COMMAND_SET) + 1;
        //解析字符串参数
        strncpy(parameter->parameterStr, cat, TSS_COMMAND_PARAMAX * 4 - 1);
        //解析32位参数
        for (uint8_t i = 0; i < TSS_COMMAND_PARAMAX; i++)
        {
            if (cat != NULL && cat != (char *)1)
            {
                parameter->parameter32[i] = strtoul(cat, NULL, 0);
                parameter->parameter32count++;
            }
            else
            {
                break;
            }
            cat = strstr(cat, TSS_COMMAND_CAT) + 1;
        }
    }
}

/**
  * @brief  at命令处理函数
  * @param  command     命令字符串的首地址
  * @param  len         命令的长度
  * @retval 返回是否成功识别到命令
  * @note   识别到命令, 直接执行命令函数
  *         本函数需要放置在数据接收函数中
  *         一条一条地将数据传入
  */
int8_t TSS_CommandProcess(uint8_t *command, uint16_t len)
{
    if (TSS_Command.table != 0)
    {
        //判断命令头
        if (strncasecmp((char *)command, (char *)TSS_COMMAND_HEAD, TSS_COMMAND_HLEN) == 0)
        {
            //遍历执行命令列表
            for (uint16_t i = 0; i < TSS_Command.num; i++)
            {
                char *msg = (char *)TSS_Command.table[i].command;
                //寻找需要执行的命令
                if (strncasecmp((char *)&command[TSS_COMMAND_HLEN], msg, strlen(msg)) == 0)
                {
                    TSS_CommandParameterDef parameter = {0};
                    TSS_CommandParameter(&command[TSS_COMMAND_HLEN], len - TSS_COMMAND_HLEN, &parameter);
                    TSS_Command.table[i].function(&parameter);
                    return TSS_OK;
                }
            }
            return TSS_NONEAT;
        }
        else //不是定义的命令
        {
            return TSS_ATERROR;
        }
    }
    return TSS_ERROR;
}
