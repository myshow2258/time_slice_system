#ifndef __TSS_Action_H
#define __TSS_Action_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

    enum
    {
        TSS_ACTIONFUN_OK = 0,
        TSS_ACTIONFUN_END
    };

    typedef int8_t (*TSS_ActionFun)(void *);

    //动作列表结构体
    typedef struct
    {
        TSS_ActionFun start_fun;     //开始函数
        TSS_ActionFun run_fun;       //运行函数
        uint16_t cycle;              //运行周期ms
        TSS_ActionFun condition_fun; //结束条件函数
        TSS_ActionFun end_fun;       //结束函数
        void *parameter;             //参数
    } TSS_ActionTableDef;

    enum
    {
        TSS_ACTION_BLOCK = 0,
        TSS_ACTION_START,
        TSS_ACTION_RUN,
        TSS_ACTION_END
    };

#define TSS_ACTION_TIMEMAX 0xFFFFFFFF
    //动作播放器状态
    typedef struct
    {
        uint8_t status;            //状态标志
        uint32_t play_time;        //上次运行函数执行时间
        uint16_t play_node;        //当前播放节点
        TSS_ActionTableDef *table; //需要播放的动作列表
        uint16_t play_len;         //动作组长度
        uint32_t times;            //重复循环次数
    } TSS_ActionPlayerDef;

    uint8_t TSS_ActionCreate(TSS_ActionPlayerDef *Action, uint8_t Priority);
    int8_t TSS_ActionStart(TSS_ActionPlayerDef *Action, TSS_ActionTableDef *Table, uint16_t Len, uint32_t Times);
    void TSS_ActionStop(TSS_ActionPlayerDef *Action);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
