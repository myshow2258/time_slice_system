#ifndef __TSS_Config_H
#define __TSS_Config_H
#ifdef __cplusplus
extern "C"
{
#endif

#if defined(STM32F10X_LD) || defined(STM32F10X_LD_VL) || defined(STM32F10X_MD) || defined(STM32F10X_MD_VL) || defined(STM32F10X_HD) || \
    defined(STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL)
#define STM32F10x
#include "stm32f10x.h"
#elif defined(STM32F40_41xxx) || defined(STM32F427_437xx) || defined(STM32F429_439xx) || defined(STM32F401xx) || defined(STM32F410xx) || \
    defined(STM32F411xE) || defined(STM32F412xG) || defined(STM32F413_423xx) || defined(STM32F446xx) || defined(STM32F469_479xx)
#define STM32F4xx
#include "stm32f4xx.h"
#endif

#define TSS_SystickHandler SysTick_Handler

#define TSS_TASKNUMMAX 32 //设置最大支持的任务个数
#define TSS_SYSHZ 1000    //设置系统运行频率
#define TSS_TRIGGERNOW 0  //触发式任务立刻执行或受调度器管理执行
#define TSS_IDLETASK 0    //设置是否开启空闲任务

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
