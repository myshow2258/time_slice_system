#ifndef __TSS_Pid_H
#define __TSS_Pid_H

#include <stdint.h>

//================================================================
// Types
//================================================================
typedef enum
{
    DISABLED = 0,
    ENABLED
} TSS_Pid_Feature;

typedef enum
{
    MANUAL = 0,
    AUTOMATIC,
    RELAY,
    OFF
} TSS_Pid_Mode;

typedef struct
{
    float PV_old;
    float Ui_old;
    float Ud_old;
    float SP_old;
} TSS_Pid_Context;

typedef struct
{

    // Parameters:
    float K, Ti, Td; // For use in NON-INT or INT modes

    float OutputMax; // For windup
    float OutputMin; // For windup

    float Nd;   // For derivator
    float b, c; // For setpoint Weighting

    float Ts; // General propoerty

    // Features:
    TSS_Pid_Mode Mode;
    TSS_Pid_Feature AntiWindup;
    TSS_Pid_Feature Bumpless;

    TSS_Pid_Context ctx;

} TSS_PidDef;

//================================================================
// Defines
//================================================================
#define EPSILON 0.0000001f

//================================================================
// Prototypes
//================================================================
void TSS_PidInit(TSS_PidDef *q);
float TSS_PidProcess_(TSS_PidDef *q, float Input, float pv, float terms[]);
#define TSS_PidProcess(pPID, input, pv) TSS_PidProcess_(pPID, input, pv, NULL);

#endif
