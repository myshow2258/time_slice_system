#ifndef __TSS_Command_H
#define __TSS_Command_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

#define TSS_COMMAND_PARAMAX 8 //最大支持的参数个数
    //AT命令参数
    typedef struct
    {
        uint32_t parameter32[TSS_COMMAND_PARAMAX];  //32位的参数
        uint32_t parameter32count;                  //32位参数的长度
        char parameterStr[TSS_COMMAND_PARAMAX * 4]; //字符串参数
    } TSS_CommandParameterDef;

    typedef uint8_t (*TSS_Command_Fun)(TSS_CommandParameterDef *parameter);

#define TSS_COMMAND_LEN 128
    //AT命令集列表
    typedef struct
    {
        uint8_t command[TSS_COMMAND_LEN]; //命令
        TSS_Command_Fun function;         //执行函数
    } TSS_CommandTableDef;

    //AT命令结构体
    typedef struct
    {
        uint16_t num;               //命令个数
        TSS_CommandTableDef *table; //命令集
    } TSS_CommandDef;

    void TSS_CommandInit(TSS_CommandTableDef *table, uint16_t num);
    int8_t TSS_CommandProcess(uint8_t *command, uint16_t len);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
