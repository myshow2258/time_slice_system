#ifndef __TSS_Task_H
#define __TSS_Task_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

    /**
	 * @brief 任务延时状态定义
	 */
    enum
    {
        DELAY_TICK = 0,
        DELAY_CYCLE,
    };
    /**
	 * @brief 任务状态枚举定义
	 */
    typedef enum
    {
        TASK_Running = 0, //正在执行的任务
        TASK_Ready,       //准备执行的任务
        TASK_Blocked,     //阻塞的任务
        TASK_Suspended,   //挂起的任务
        TASK_Deleted,     //删除的任务
        TASK_Invalid,     //忽略的任务
    } TSS_TaskStatus;

    /**
	 * @brief 任务类型枚举定义
	 */
    typedef enum
    {
        TASK_HZ = 0,  //按照设定的执行频率不断执行
        TASK_TRIGGER, //触发异步执行
    } TSS_TaskTypes;

    /**
	 * @brief 任务消息结构体
	 */
    typedef struct
    {
        int32_t past;     //任务延迟时间
        uint8_t effect;   //消息有效位，消息发出后为1，任务执行一次置0
        uint32_t message; //消息
        void *data;       //数据指针
    } TSS_TaskMessageDef;

    typedef void (*TSS_TaskFunction)(TSS_TaskMessageDef *);

    /**
	 * @brief 任务信息结构体
	 */
    typedef struct
    {
        uint8_t ID;                 //任务序号
        volatile int32_t Delay[2];  //任务剩余延时时间
        volatile uint8_t Status;    //任务状态
        uint8_t Priority;           //优先级
        TSS_TaskTypes Types;        //任务类型
        TSS_TaskFunction Callback;  //任务函数指针
        TSS_TaskMessageDef Message; //任务消息
    } TSS_TaskInfoDef;

    /**
	 * @brief 任务列表结构体
	 */
    typedef struct
    {
        uint8_t TASK_Count;            //任务计数
        volatile uint32_t TSS_RunTime; //系统运行时间
        TSS_TaskInfoDef TASK_List[TSS_TASKNUMMAX];
    } TSS_TaskList;

    void TSS_Init(void);
    void TSS_Delay(uint32_t ms);
    uint32_t TSS_GetRunTime(void);
    void TSS_SystickHandler(void);
    void TSS_IdleTask(void *p);

    uint8_t TSS_ThreadCreate(TSS_TaskFunction Callback, uint8_t Priority, TSS_TaskTypes Types, int32_t Delay);
    void TSS_ThreadSetTrigger(uint8_t ThreadID, int32_t Delay);
    void TSS_ThreadDelayChange(uint8_t ThreadID, int32_t Delay);
    void TSS_ThreadPutMessage(uint8_t ThreadID, TSS_TaskMessageDef message);
    void TSS_Start(void);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
