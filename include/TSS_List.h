#ifndef __TSS_List_H
#define __TSS_List_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

    int8_t TSS_ListInit(void *list, uint16_t size);

    int8_t TSS_ListInsert(void *list, void *data, uint16_t len, uint16_t num);
    int8_t TSS_ListErase(void *list, uint16_t num);
    int8_t TSS_ListAccess(void *list, void *data, uint16_t *len, uint16_t num);

    uint16_t TSS_ListSize(void *list);
    int32_t TSS_ListSpace(void *list);
    int8_t TSS_ListEmpty(void *list);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
