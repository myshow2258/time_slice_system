#ifndef __TSS_Queue_H
#define __TSS_Queue_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

    int8_t TSS_QueueInit(void *p, uint16_t size);

    int8_t TSS_QueueFront(void *queue, void *data, uint16_t *len);
    int8_t TSS_QueueBack(void *queue, void *data, uint16_t *len);
    int8_t TSS_QueuePush(void *queue, void *data, uint16_t len);
    int8_t TSS_QueuePop(void *queue);

    uint16_t TSS_QueueSize(void *queue);
    int8_t TSS_QueueEmpty(void *queue);
    int32_t TSS_QueueSpace(void *queue);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
