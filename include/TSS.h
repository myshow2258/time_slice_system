#ifndef __TSS_H
#define __TSS_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "TSS_Config.h"
#include "TSS_Task.h"
#include "TSS_Queue.h"
#include "TSS_List.h"
#include "TSS_Memery.h"
#include "TSS_Action.h"
#include "TSS_Command.h"
#include "TSS_Modbus.h"
#include "TSS_Pid.h"

#define TSS_MAX_DELAY (0x7FFFFFFEU) //定义最大延时时间

    /**
     * @brief 函数返回值枚举定义
     */
    typedef enum
    {
        TSS_OK = 0,  //正常
        TSS_MEMSIZE, //内存大小异常
        TSS_MEMFULL, //内存溢出
        TSS_NODATA,  //没有数据
        TSS_ATERROR, //命令错误
        TSS_NONEAT,  //没有这条命令

        TSS_ERROR, //错误
    } TSS_ReTypeDef;

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
