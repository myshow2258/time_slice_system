#ifndef __TSS_Memery_H
#define __TSS_Memery_H
#ifdef __cplusplus
extern "C"
{
#endif

#include "TSS.h"

    int8_t TSS_MemInit(void *mem, uint16_t size);

    void *TSS_MemMalloc(void *mem, uint16_t size);
    int8_t TSS_MemFree(void *mem, void *p);

    uint16_t TSS_MemSize(void *mem);
    int32_t TSS_MemSpace(void *mem);

#ifdef __cplusplus
}
#endif
#endif /*__XXX_H */
